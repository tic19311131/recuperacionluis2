import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, Alert, TouchableHighlight} from 'react-native';
import Input from './components/Data';

export default function App() {
  const MxDll = () =>{
    alert('$ 20 = 1 US');
  }

  return (
    <View style={styles.container}>
      <TouchableHighlight onPress={MxDll}>
        <Image source={{
            uri: 'https://es.calcuworld.com/wp-content/uploads/sites/2/2019/11/peso-mexicano-a-dolar.png',
          }}
          style={{
            width: 50,
            height: 50,
            marginTop:20,
          }}
          />
        </TouchableHighlight>
      <Input />
      <StatusBar barStyle="dark-content" backgroundColor="#ff0000" hidden={false}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});
