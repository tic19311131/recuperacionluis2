import React, { useState } from "react";
import { StyleSheet, ScrollView, View, Text, TextInput, Button,TouchableHighlight, Image} from 'react-native';




const Input = () => {
    const [pesos, setPesos] = useState(" ");
    const [dollars, setDollars] = useState(" ");

    const convertMxDll = () =>{
        setDollars(pesos/20);
    }
    const convertDllMx = () =>{
        setPesos(dollars*20);
    }
    const DllMx = () =>{
        alert('1 US = 20 MX');
      }
    

    return (
        <View style={styles.container} >
            
            <Text>Mexican Pesos</Text>
            <TextInput style={styles.input} onChangeText={setPesos} value={pesos}/>
            <Button onPress={convertMxDll} title="Convert MX-US" color="#0000FF"/>
            <TouchableHighlight onPress={DllMx}>
        <Image source={{
            uri: 'https://www.featurepics.com/StockImage/20070827/us-dollar-symbol-stock-illustration-430633.jpg',
          }}
          style={{
            width: 50,
            height: 50,
            marginTop:20,
          }}
          />
        </TouchableHighlight>
          
            <Text>US Dollars</Text>
           
            <TextInput style={styles.input} onChangeText={setDollars} value={dollars}/>
           
            <Button onPress={convertDllMx} title="Convert US-MX" color="#658135"/>

        </View>
    )
}

const styles = StyleSheet.create({
    input: {
      height: 40,
      margin: 12,
      borderWidth: 1,
      alignItems:'center',
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems:'center',
      },
  });

export default Input;